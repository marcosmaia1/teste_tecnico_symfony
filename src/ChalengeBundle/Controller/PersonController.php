<?php

namespace ChalengeBundle\Controller;

use ChalengeBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Person controller.
 *
 * @Route("person")
 */
class PersonController extends Controller
{
    /**
     * Lists all person entities.
     *
     * @Route("/", name="person_index")
     * @Method("GET")
     */
    public function indexAction($message = null, $openModal = 0)
    {
        $em = $this->getDoctrine()->getManager();

        $people = $em->getRepository('ChalengeBundle:Person')->findAll();

        return $this->render('person/index.html.twig', array(
            'people' => $people,
            'message' => $message,
            'openModal' => $openModal
        ));
    }

    /**
     * Lists all person entities.
     *
     * @Route("/", name="person_modal_index")
     * @Method("GET")
     */
    public function indexModalAction()
    {
        return $this->indexAction($openModal = 1);
    }

    /**
     * Finds and displays a person entity.
     *
     * @Route("/{id}", name="person_show")
     * @Method("GET")
     */
    public function showAction(Person $person)
    {
        return $this->render('person/show.html.twig', array(
            'person' => $person,
        ));
    }

    /**
     * Finds and displays a person entity.
     *
     * @Route("/", name="person_upload")
     * @Method("POST")
     */
    public function uploadAction(Request $request)
    {
        $csv = $request->files->get('xml_file');
        $fileName = $csv->getClientOriginalName();
        $fileExtension = $csv->getClientOriginalExtension();

        if($csv->getClientOriginalExtension() != 'xml'){
            print_r('Formato inválido.');
            die;
        } else {
            $xmlDataArray = $this->ReadSaveXMLFile($csv);
        }

        return $this->indexAction($message=$xmlDataArray);
    }

    public function ReadSaveXMLFile($file){
        $em = $this->getDoctrine()->getManager();

        $dom = new \DOMDocument('1.0', 'UTF-8');
	    $dom->load($file);
        $people = $dom->getElementsByTagName('person');

        foreach ($people as $person) {
            $personEntity = new Person();
            $personEntity->setPersonid($person->getElementsByTagName('personid')->item(0)->nodeValue);
            $personEntity->setPersonname($person->getElementsByTagName('personname')->item(0)->nodeValue);
            $phone = $person->getElementsByTagName('phone');
            $phoneArray = [];
            foreach($phone as $phone){
                $phoneArray[] = $phone->nodeValue;
            }
            $personEntity->setPhones($phoneArray);
            $em->persist($personEntity);
        }
        $em->flush();
        return 'Importação realizada';
    }

    //REST API METHODS

    /** *
     * @Rest\View
     */
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $people = $em->getRepository('ChalengeBundle:Person')->findAll();
        return array('people' => $people);
    }

    /**
     * @Rest\View
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $person = $em->getRepository('ChalengeBundle:Person')->findBy(array('personid' => $id));
        return array('person' => $person);
    }
}
