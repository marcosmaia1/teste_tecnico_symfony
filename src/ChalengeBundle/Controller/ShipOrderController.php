<?php

namespace ChalengeBundle\Controller;

use ChalengeBundle\Entity\ShipOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Shiporder controller.
 *
 * @Route("shiporder")
 */
class ShipOrderController extends Controller
{
    /**
     * Lists all shipOrder entities.
     *
     * @Route("/", name="shiporder_index")
     * @Method("GET")
     */
    public function indexAction($message = null, $openModal = 0)
    {
        $em = $this->getDoctrine()->getManager();

        $shipOrders = $em->getRepository('ChalengeBundle:ShipOrder')->findAll();

        return $this->render('shiporder/index.html.twig', array(
            'shipOrders' => $shipOrders,
            'message' => $message,
            'openModal' => $openModal
        ));
    }

    /**
     * Finds and displays a shipOrder entity.
     *
     * @Route("/{id}", name="shiporder_show")
     * @Method("GET")
     */
    public function showAction(ShipOrder $shipOrder)
    {
        return $this->render('shiporder/show.html.twig', array(
            'shipOrder' => $shipOrder,
        ));
    }

    /**
     * Finds and displays a person entity.
     *
     * @Route("/", name="shiporder_upload")
     * @Method("POST")
     */
    public function uploadAction(Request $request)
    {
        $csv = $request->files->get('xml_file');
        $fileName = $csv->getClientOriginalName();
        $fileExtension = $csv->getClientOriginalExtension();

        if($csv->getClientOriginalExtension() != 'xml'){
            print_r('Formato inválido.');
            die;
        } else {
            $xmlDataArray = $this->ReadSaveXMLFile($csv);
        }
        return $this->indexAction($message=$xmlDataArray);
    }

    public function ReadSaveXMLFile($file){
        $em = $this->getDoctrine()->getManager();

        $dom = new \DOMDocument('1.0', 'UTF-8');
	    $dom->load($file);
        $shipOrder = $dom->getElementsByTagName('shiporder');
        foreach ($shipOrder as $order) {
            $shipOrderEntity = new ShipOrder();
            $shipOrderEntity->setOrderid($order->getElementsByTagName('orderid')->item(0)->nodeValue);
            $shipOrderEntity->setOrderperson($order->getElementsByTagName('orderperson')->item(0)->nodeValue);
            $shipTo = $order->getElementsByTagName('shipto');
            $shipToArray = array();
            foreach($shipTo as $node){
                $shipToArray['name'] = $node->getElementsByTagName('name')->item(0)->nodeValue;
                $shipToArray['address'] = $node->getElementsByTagName('address')->item(0)->nodeValue;
                $shipToArray['city'] = $node->getElementsByTagName('city')->item(0)->nodeValue;
                $shipToArray['country'] = $node->getElementsByTagName('country')->item(0)->nodeValue;
            }
            $shipOrderEntity->setShipto($shipToArray);
            $items = $order->getElementsByTagName('item');
            $itemsArray = array(
                'items' => []
            );
            foreach($items as $item){
                $itemArray = array();
                $itemArray['title'] = $item->getElementsByTagName('title')->item(0)->nodeValue;
                $itemArray['note'] = $item->getElementsByTagName('note')->item(0)->nodeValue;
                $itemArray['quantity'] = $item->getElementsByTagName('quantity')->item(0)->nodeValue;
                $itemArray['price'] = $item->getElementsByTagName('price')->item(0)->nodeValue;
                $itemsArray['items'][] = $itemArray;
            }
            $shipOrderEntity->setItems($itemsArray);
            $em->persist($shipOrderEntity);
        }
        $em->flush();
        return 'Importação realizada';
    }

    //REST API METHODS

    /** *
     * @Rest\View
     */
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $shiporder = $em->getRepository('ChalengeBundle:ShipOrder')->findAll();
        return array('shiporder' => $shiporder);
    }

    /**
     * @Rest\View
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $shiporder = $em->getRepository('ChalengeBundle:ShipOrder')->findBy(array('orderid' => $id));
        return array('shiporder' => $shiporder);
    }
}
