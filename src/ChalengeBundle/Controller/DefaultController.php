<?php

namespace ChalengeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function loginAction()
    {
        return $this->render('ChalengeBundle:Default:Login.html.twig');
    }

    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        return $this->render('ChalengeBundle:Default:index.html.twig', array('name' => 'teste'));
    }
}
