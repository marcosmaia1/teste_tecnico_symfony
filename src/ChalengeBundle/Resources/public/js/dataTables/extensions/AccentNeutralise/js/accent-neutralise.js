/**
 * When search a table with accented characters, it can be frustrating to have
 * an input such as _Zurich_ not match _Zürich_ in the table (`u !== ü`). This
 * type based search plug-in replaces the built-in string formatter in
 * DataTables with a function that will remove replace the accented characters
 * with their unaccented counterparts for fast and easy filtering.
 *
 * Note that with the accented characters being replaced, a search input using
 * accented characters will no longer match. The second example below shows
 * how the function can be used to remove accents from the search input as well,
 * to mitigate this problem.
 *
 *  @summary Replace accented characters with unaccented counterparts
 *  @name Accent neutralise
 *  @author Allan Jardine
 *
 *  @example
 *    $(document).ready(function() {
 *        $('#example').dataTable();
 *    } );
 *
 *  @example
 *    $(document).ready(function() {
 *        var table = $('#example').dataTable();
 *
 *        // Remove accented character from search input as well
 *        $('#myInput').keyup( function () {
 *          table
 *            .search(
 *              jQuery.fn.DataTable.ext.type.search.string( this.value )
 *            )
 *            .draw()
 *        } );
 *    } );
 */

(function(){

function removeAccents ( data ) {
    return data
    .replace(/[áÁàÀâÂäÄãÃåÅæÆ]/g, 'a')
    .replace(/[çÇ]/g, 'c')
    .replace(/[éÉèÈêÊëË]/g, 'e')
    .replace(/[íÍìÌîÎïÏîĩĨĬĭ]/g, 'i')
    .replace(/[ñÑ]/g, 'n')
    .replace(/[óÓòÒôÔöÖœŒ]/g, 'o')
    .replace(/[ß]/g, 's')
    .replace(/[úÚùÙûÛüÜ]/g, 'u')
    .replace(/[ýÝŷŶŸÿ]/g, 'n')
    .replace(/[üÜ]/g, 'u')
    .replace(/[áÁàÀâÂäÄãÃåÅæÆ]/g, 'a')
    .replace(/[çÇ]/g, 'c')
    .replace(/[éÉèÈêÊëË]/g, 'e')
    .replace(/[íÍìÌîÎïÏîĩĨĬĭ]/g, 'i')
    .replace(/[ñÑ]/g, 'n')
    .replace(/[óÓòÒôÔöÖœŒ]/g, 'o')
    .replace(/[ß]/g, 's')
    .replace(/[úÚùÙûÛüÜ]/g, 'u')
    .replace(/[ýÝŷŶŸÿ]/g, 'n');
}

var searchType = jQuery.fn.DataTable.ext.type.search;

searchType.string = function ( data ) {
    return ! data ?
        '' :
        typeof data === 'string' ?
            removeAccents( data ) :
            data;
};

searchType.html = function ( data ) {
    return ! data ?
        '' :
        typeof data === 'string' ?
            removeAccents( data.replace( /<.*?>/g, '' ) ) :
            data;
};

}());
