<?php

namespace ChalengeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="ChalengeBundle\Repository\PersonRepository")
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="personid", type="string", length=10, unique=true)
     */
    private $personid;

    /**
     * @var string
     *
     * @ORM\Column(name="personname", type="string", length=100)
     */
    private $personname;

    /**
     * @var array
     *
     * @ORM\Column(name="phones", type="array")
     */
    private $phones;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personid
     *
     * @param string $personid
     *
     * @return Person
     */
    public function setPersonid($personid)
    {
        $this->personid = $personid;

        return $this;
    }

    /**
     * Get personid
     *
     * @return string
     */
    public function getPersonid()
    {
        return $this->personid;
    }

    /**
     * Set personname
     *
     * @param string $personname
     *
     * @return Person
     */
    public function setPersonname($personname)
    {
        $this->personname = $personname;

        return $this;
    }

    /**
     * Get personname
     *
     * @return string
     */
    public function getPersonname()
    {
        return $this->personname;
    }

    /**
     * Set phones
     *
     * @param array $phones
     *
     * @return Person
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return array
     */
    public function getPhones()
    {
        return $this->phones;
    }
}
