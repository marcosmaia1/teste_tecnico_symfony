<?php

namespace ChalengeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShipOrder
 *
 * @ORM\Table(name="ship_order")
 * @ORM\Entity(repositoryClass="ChalengeBundle\Repository\ShipOrderRepository")
 */
class ShipOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="orderid", type="string", length=10, unique=true)
     */
    private $orderid;

    /**
     * @var string
     *
     * @ORM\Column(name="orderperson", type="string", length=10)
     */
    private $orderperson;

    /**
     * @var json
     *
     * @ORM\Column(name="shipto", type="json")
     */
    private $shipto;

    /**
     * @var json
     *
     * @ORM\Column(name="items", type="json")
     */
    private $items;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderid
     *
     * @param string $orderid
     *
     * @return ShipOrder
     */
    public function setOrderid($orderid)
    {
        $this->orderid = $orderid;

        return $this;
    }

    /**
     * Get orderid
     *
     * @return string
     */
    public function getOrderid()
    {
        return $this->orderid;
    }

    /**
     * Set orderperson
     *
     * @param string $orderperson
     *
     * @return ShipOrder
     */
    public function setOrderperson($orderperson)
    {
        $this->orderperson = $orderperson;

        return $this;
    }

    /**
     * Get orderperson
     *
     * @return string
     */
    public function getOrderperson()
    {
        return $this->orderperson;
    }

    /**
     * Set shipto
     *
     * @param json $shipto
     *
     * @return ShipOrder
     */
    public function setShipto($shipto)
    {
        $this->shipto = $shipto;

        return $this;
    }

    /**
     * Get shipto
     *
     * @return json
     */
    public function getShipto()
    {
        return $this->shipto;
    }

    /**
     * Set items
     *
     * @param json $items
     *
     * @return ShipOrder
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * Get items
     *
     * @return json
     */
    public function getItems()
    {
        return $this->items;
    }
}
