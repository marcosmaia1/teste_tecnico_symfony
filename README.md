teste_tecnico_symfony2 Instalation
======================


1. Clone project

2. Run composer update

3. Navigate to the project folder

4. Run php app/console doctrine:schema:create for database creation

5. Run php app/console server:run
(Default link its http://127.0.0.1:8000)

Rest API Methods:
======================

{{Default link}}/api/shiporder
- Return all shiporder data

{{Default link}}/api/shiporder/{orderid}
- Return single order data

{{Default link}}/api/person
- Return all person data

{{Default link}}/api/person/{personid}
- Return single person data
